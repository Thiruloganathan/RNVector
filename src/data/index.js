import Colors from '../resources/colors';
export default data = {
    ColorsArray: [
        {
            code: Colors.black,
            name: "Black",
            id: 1,
        },
        {
            code: Colors.lightGreen,
            name: "LightGreen",
            id: 2,
        },
        {
            code: Colors.darkGreen,
            name: "DarkGreen",
            id: 3,
        },
        {
            code: Colors.brownishOrange,
            name: "BrownishOrange",
            id: 4,
        },
        {
            code: Colors.twitter,
            name: "Twitter",
            id: 5,
        },
        {
            code: Colors.yellow,
            name: "Yellow",
            id: 6,
        },
        {
            code: Colors.gray,
            name: "Gray",
            id: 7,
        },
        {
            code: Colors.blueGray,
            name: "BlueGray",
            id: 8,
        },
        {
            code: Colors.middleRed,
            name: "MiddleRed",
            id: 9,
        },
        {
            code: Colors.middleBlue,
            name: "MiddleBlue",
            id: 10,
        },
        {
            code: Colors.lightBlue,
            name: "LightBlue",
            id: 11,
        },
        {
            code: Colors.lime,
            name: "Lime",
            id: 12,
        },
        {
            code: Colors.purple,
            name: "Purple",
            id: 13,
        },
        {
            code: Colors.aqua,
            name: "Aqua",
            id: 14,
        },
        {
            code: Colors.maroon,
            name: "Maroon",
            id: 15,
        },

    ],
    IconsArray: [
        {
            id: 1,
            name: "app-store"
        },
        {
            id: 2,
            name: "bell"
        },
        {
            id: 3,
            name: "cake"
        },
        {
            id: 4,
            name: "compass"
        },
        {
            id: 5,
            name: "database"
        },
        {
            id: 6,
            name: "emoji-happy"
        },
        {
            id: 7,
            name: "fingerprint"
        },
        {
            id: 8,
            name: "flower"
        },
        {
            id: 9,
            name: "heart"
        },
        {
            id: 10,
            name: "game-controller"
        },
        {
            id: 11,
            name: "globe"
        },
        {
            id: 12,
            name: "hand"
        },
        {
            id: 13,
            name: "pie-chart"
        },
        {
            id: 14,
            name: "signal"
        },
        {
            id: 15,
            name: "thumbs-up"
        },
    ]

};

