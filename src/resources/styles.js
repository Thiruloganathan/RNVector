import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    HomeContainer: {
        flex: 1,
        paddingVertical: 10, paddingHorizontal: 10
    },
    HomeParent: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    HomeHeaderIcon: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    HomeHeaderIconList: {
        justifyContent: 'flex-start',
        alignItems: "flex-end"
    },
    HomeColorsContainer: { flex: 1, flexDirection: 'row', paddingVertical: 20, justifyContent: 'center', alignItems: 'center', flexWrap: 'wrap' },
    HomeColorsParent: { marginTop: 10, },
    HomeColorsItem: (bgClr) => ({ width: 50, height: 50, backgroundColor: bgClr, marginEnd: 5, marginStart: 5, marginTop: 10, borderRadius: 10 }),
    HomeColorsItemSelect: {
        position: 'absolute',
        flex: 1,
        width: 20,
        height: 20,
        alignSelf: 'center',
        alignItems: 'center'
    },
    HomeModalContainer: {
        justifyContent: 'flex-end',
        margin: 0,
        marginTop: 100,
    },
    HomeModalParent: {
        flex: 1, backgroundColor: 'white', borderTopEndRadius: 20,
        borderTopStartRadius: 20
    },
    HomeModalTitle: { textAlign: 'center', paddingTop: 10, fontSize: 18 },
    HomeModalClose: { position: 'absolute', width: '100%', alignItems: 'flex-end', marginTop: 10 },
    HomeModalIconsList: { flex: 1, flexDirection: 'row', paddingVertical: 20, },
    HomeModalIconItem: { flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 20 },
    HomeModalIcon: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },
    HomeModalIconSelect: {
        position: 'absolute',
        flex: 1,
        width: 20,
        height: 20,
        alignSelf: 'flex-end',
    },

});

export default styles;