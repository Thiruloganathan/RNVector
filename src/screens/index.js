import React, { memo, useState } from 'react';
import { SafeAreaView, View, TouchableOpacity, Text, Image, FlatList } from 'react-native';
import Styles from '../resources/styles';
import VectorIcon from '../components/VectorIcons';
import Data from '../data';
import Modal from 'react-native-modal';
import * as Animatable from 'react-native-animatable';

const Home = () => {
    const [vectorClr, setVectorClr] = useState('#000000');
    const [vectorName, setVectorName] = useState('app-store');
    const [isModalVisible, setModalVisible] = useState(false);
    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };
    return (
        <SafeAreaView style={Styles.HomeContainer}>
            <View style={Styles.HomeParent}>
                <VectorIcon bgClr={vectorClr} value={vectorName} iconSize={100} />
                <View style={Styles.HomeHeaderIcon}>
                    <TouchableOpacity style={Styles.HomeHeaderIconList}
                        onPress={toggleModal}
                    >
                        <VectorIcon bgClr={'black'} value={'archive'} iconSize={25} />
                    </TouchableOpacity>
                </View>

            </View>
            <View style={Styles.HomeColorsContainer}>
                {Data.ColorsArray && Data.ColorsArray.map((item, index) => {
                    return (
                        <Animatable.View
                            animation='fadeInRight'
                            key={index}
                            delay={500 + index * 100}
                        >
                            <TouchableOpacity key={index}
                                onPress={() => setVectorClr(item.code)}
                            >
                                <View style={Styles.HomeColorsParent}>
                                    <View key={index} style={Styles.HomeColorsItem(item.code)} />
                                    {item.code == vectorClr && <Image
                                        source={require('../resources/images/ic_check.png')}
                                        style={Styles.HomeColorsItemSelect}
                                    />}
                                </View>
                            </TouchableOpacity>
                        </Animatable.View>
                    )
                })}
            </View>
            {
                isModalVisible && <Modal
                    isVisible={isModalVisible}
                    onSwipeComplete={toggleModal}
                    onBackdropPress={toggleModal}
                    swipeDirection={['down']}
                    animationIn="slideInUp"
                    animationOut="slideOutDown"
                    useNativeDriver={true}
                    style={Styles.HomeModalContainer}
                >
                    <View style={Styles.HomeModalParent}>
                        <View>
                            <Text style={Styles.HomeModalTitle}>Choose Icon</Text>
                            <View style={Styles.HomeModalClose}>
                                <TouchableOpacity style={{ marginEnd: 10 }}
                                    onPress={toggleModal}
                                >
                                    <VectorIcon iconSize={23} value={"circle-with-cross"} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={Styles.HomeModalIconsList}>
                            <FlatList
                                data={Data.IconsArray}
                                numColumns={3}
                                renderItem={({ item, index }) => {
                                    return (
                                        <Animatable.View
                                            style={Styles.HomeModalIconItem}
                                            animation='bounceIn'
                                            key={index}
                                            delay={300 + index * 100}
                                        >
                                            <TouchableOpacity
                                                style={Styles.HomeModalIcon}
                                                key={index}
                                                onPress={() => {
                                                    setVectorName(item.name);
                                                    setModalVisible(!isModalVisible);
                                                }}
                                            >
                                                <VectorIcon
                                                    bgClr={'black'}
                                                    value={item.name}
                                                    iconSize={35}
                                                />
                                                {item.name == vectorName && <Image
                                                    source={require('../resources/images/ic_check.png')}
                                                    style={Styles.HomeModalIconSelect}
                                                />}
                                            </TouchableOpacity>
                                        </Animatable.View>
                                    )
                                }}
                            />
                        </View>
                    </View>
                </Modal>
            }
        </SafeAreaView>
    );
}

export default memo(Home);