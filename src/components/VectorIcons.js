import React from 'react';
import { View } from 'react-native';
import Colors from '../resources/colors';
import Icon from 'react-native-vector-icons/Entypo';

const VectorIcons = ({bgClr,value,iconSize,isSelected}) => {
    return (
       <View>
           <Icon 
           name={value ? value : "app-store"} 
           size={iconSize} 
           color={isSelected ? Colors.brownishOrange : bgClr ? bgClr :  Colors.black}
           />
       </View>
    );
}

export default VectorIcons;